package de.thi.jbsa.main.repository;

import de.thi.jbsa.main.domain.City;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class CityRepositoryTest {

  @Autowired
  private CityRepository cityRepository;

  @Test
  public void createAndReadCity() {
    City city = new City();
    city.setName("Ingolstadt");
    city.setInhabitants(133638L);
    City savedCity = cityRepository.save(city);

    assertEquals("Ingolstadt", savedCity.getName());

    City foundCity = cityRepository.findByName("Ingolstadt");
    assertEquals((Long) 133638L, foundCity.getInhabitants());
  }

  @After
  public void tearDown()
    throws Exception {
    cityRepository.deleteAll();
  }
}
