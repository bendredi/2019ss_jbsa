package de.thi.jbsa.main.eventreceiver.preprocessor;

import de.thi.jbsa.main.domain.PublishMessageCmd;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SwearWordFilterPreProcessorTest {

    @InjectMocks
    public SwearWordFilterPreProcessor swearWordFilterPreProcessor;

    @Test
    public void ensureMessagesWithoutSwearWordsAreNotChanged(){

        PublishMessageCmd publishMessageCmd = new PublishMessageCmd();
        publishMessageCmd.setMessage("Hallo Welt");
        publishMessageCmd.setChannelId("Test Channel");
        publishMessageCmd.setUserId("Karl Peter");

        Assert.assertTrue(swearWordFilterPreProcessor.processPublishMessageCmd(publishMessageCmd));
        Assert.assertEquals("Hallo Welt", publishMessageCmd.getMessage());
        Assert.assertEquals("Test Channel", publishMessageCmd.getChannelId());
        Assert.assertEquals("Karl Peter", publishMessageCmd.getUserId());
    }

    @Test
    public void ensureMessagesWithSwearWordsAreChanged(){

        PublishMessageCmd publishMessageCmd = new PublishMessageCmd();
        publishMessageCmd.setMessage("So ein Mist!");
        publishMessageCmd.setChannelId("Test Channel");
        publishMessageCmd.setUserId("Karl Peter");

        Assert.assertTrue(swearWordFilterPreProcessor.processPublishMessageCmd(publishMessageCmd));
        Assert.assertEquals("****", publishMessageCmd.getMessage());
        Assert.assertEquals("Test Channel", publishMessageCmd.getChannelId());
        Assert.assertEquals("Karl Peter", publishMessageCmd.getUserId());
    }
}
