package de.thi.jbsa.main.repository;

import de.thi.jbsa.main.domain.ChangeInhabitantEvent;
import de.thi.jbsa.main.domain.CreateCityEvent;
import de.thi.jbsa.main.domain.Event;
import de.thi.jbsa.main.domain.EventType;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class EventRepositoryTest {

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Test
  public void saveEventInRepository() {
    ChangeInhabitantEvent changeInhabitantEvent = new ChangeInhabitantEvent();

    changeInhabitantEvent.setNewInhabitantNumber(200000L);
    changeInhabitantEvent.setAffectedCity("Ingolstadt");

    ChangeInhabitantEvent changeInhabitantEventSaved = eventRepository.save(changeInhabitantEvent);
    assertEquals((Long) 200000L, changeInhabitantEventSaved.getNewInhabitantNumber());

    Event savedEvent = eventRepository.findByEventType(EventType.CHANGE_CITY_INHABITANTS).get(0);

    assertEquals(changeInhabitantEventSaved.getId(),savedEvent.getId());
  }

  @Test
  public void createCityEvent() {
    CreateCityEvent event = new CreateCityEvent();
    event.setName("test");
    event.setInhabitantNumber(100L);
    CreateCityEvent eventsaved = eventRepository.save(event);

    Event savedEvent = eventRepository.findByEventType(EventType.PUBLISH_MESSAGE).get(0);
    assertNotNull(savedEvent);
  }

  @After
  public void tearDown()
    throws Exception {
    eventRepository.deleteAll();
  }
}
