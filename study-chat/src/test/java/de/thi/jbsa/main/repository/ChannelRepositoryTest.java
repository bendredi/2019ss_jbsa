package de.thi.jbsa.main.repository;

import de.thi.jbsa.main.domain.Channel;
import de.thi.jbsa.main.domain.Message;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDateTime.now;
import static org.junit.Assert.assertEquals;


@SpringBootTest
@RunWith(SpringRunner.class)
public class ChannelRepositoryTest {

    @Autowired
    private ChannelRepository channelRepository;

    @Test
    public void createAndReadChannel() {
        Message message = new Message();
        message.setDateSent(now());
        message.setId(200L);
        message.setMessageText("Hello World");
        List<Message> channelList = new ArrayList<>();
        channelList.add(message);

        Channel channel = new Channel();
        channel.setId(1L);
        channel.setMessageList(channelList);
        Channel savedChannel = channelRepository.save(channel);

        assertEquals("Hello World", savedChannel.getMessageList().get(0).getMessageText());

        Channel foundChannel = channelRepository.findById(1L).get();
        assertEquals((Long) 200L, foundChannel.getMessageList().get(0).getId());
    }

    @After
    public void tearDown() {
        channelRepository.deleteAll();
    }
}
