package de.thi.jbsa.main.eventreceiver;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.thi.jbsa.main.domain.PublishMessageCmd;
import de.thi.jbsa.main.eventreceiver.event.PublishedMessageEvent;
import de.thi.jbsa.main.eventreceiver.persistence.MessageEventEntity;
import de.thi.jbsa.main.eventreceiver.persistence.MessageEventRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MessagePublisherListenerTest {

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Mock
    private MessageEventRepository messageEventRepository;

    @Mock
    private MessagePreProcessor preProcessor1;

    @Mock
    private MessagePreProcessor preProcessor2;

    private MessagePublisherListener messagePublisherListener;

    @Before
    public void init(){

        doReturn(true).when(preProcessor1).processPublishMessageCmd(any());
        doReturn(true).when(preProcessor2).processPublishMessageCmd(any());

        ObjectMapper objectMapper = new ObjectMapper();

        messagePublisherListener = new MessagePublisherListener(
                messageEventRepository,
                Arrays.asList(preProcessor1, preProcessor2),
                objectMapper,
                applicationEventPublisher);
    }

    @Test
    public void ensureAllPreProcessorsAreCalled(){

        PublishMessageCmd publishMessageCmd = new PublishMessageCmd();
        publishMessageCmd.setMessage("Test Message");
        publishMessageCmd.setChannelId("Test Channel");
        publishMessageCmd.setUserId("Karl Peter");

        messagePublisherListener.onPublishMessageCmd(publishMessageCmd);

        verify(preProcessor1).processPublishMessageCmd(publishMessageCmd);
        verify(preProcessor2).processPublishMessageCmd(publishMessageCmd);
    }

    @Test
    public void ensurePreProcessorsReturningNullCancelMessagePublishing(){

        doReturn(false).when(preProcessor1).processPublishMessageCmd(any());

        PublishMessageCmd publishMessageCmd = new PublishMessageCmd();
        publishMessageCmd.setMessage("Test Message");
        publishMessageCmd.setChannelId("Test Channel");
        publishMessageCmd.setUserId("Karl Peter");

        messagePublisherListener.onPublishMessageCmd(publishMessageCmd);

        verify(preProcessor1).processPublishMessageCmd(publishMessageCmd);
        verify(preProcessor2, never()).processPublishMessageCmd(publishMessageCmd);
        verify(messageEventRepository, never()).save(any());
        verify(applicationEventPublisher, never()).publishEvent(any());
    }

    @Test
    public void ensureEventsAreSavedCorrectly(){

        ArgumentCaptor<MessageEventEntity> entityCaptor = ArgumentCaptor.forClass(MessageEventEntity.class);

        PublishMessageCmd publishMessageCmd = new PublishMessageCmd();
        publishMessageCmd.setMessage("Test Message");
        publishMessageCmd.setChannelId("Test Channel");
        publishMessageCmd.setUserId("Karl Peter");

        messagePublisherListener.onPublishMessageCmd(publishMessageCmd);

        verify(messageEventRepository).save(entityCaptor.capture());

        MessageEventEntity entity = entityCaptor.getValue();

        Assert.assertNull(entity.getId());
        Assert.assertEquals("Test Channel", entity.getChannelId());
        Assert.assertEquals("{\"message\":\"Test Message\",\"userId\":\"Karl Peter\",\"channelId\":\"Test Channel\"}", entity.getEvent());
    }

    @Test
    public void ensureCommandsAreSentAsEventsCorrectly(){

        ArgumentCaptor<PublishedMessageEvent> eventCaptor = ArgumentCaptor.forClass(PublishedMessageEvent.class);

        PublishMessageCmd publishMessageCmd = new PublishMessageCmd();
        publishMessageCmd.setMessage("Test Message");
        publishMessageCmd.setChannelId("Test Channel");
        publishMessageCmd.setUserId("Karl Peter");

        messagePublisherListener.onPublishMessageCmd(publishMessageCmd);

        verify(applicationEventPublisher).publishEvent(eventCaptor.capture());

        PublishedMessageEvent event = eventCaptor.getValue();

        Assert.assertEquals("Test Message", event.getMessage());
        Assert.assertEquals("Test Channel", event.getChannel());
        Assert.assertEquals("Karl Peter", event.getUser());
    }
}
