package de.thi.jbsa.main.domain;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */
@Document
@Data
public class City {

  private Long inhabitants;

  private String name;

  public Long getInhabitants() {
    return inhabitants;
  }

  public String getName() {
    return name;
  }

  public void setInhabitants(Long inhabitants) {
    this.inhabitants = inhabitants;
  }

  public void setName(String name) {
    this.name = name;
  }
}
