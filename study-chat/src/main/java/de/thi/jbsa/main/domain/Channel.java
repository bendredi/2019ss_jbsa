package de.thi.jbsa.main.domain;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Data
public class Channel {

    private long id;

    private List<Message> messageList;

}
