package de.thi.jbsa.main.wordfilter;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import de.thi.jbsa.main.domain.PublishMessageCmd;

@Aspect
public class WordFilter {
	@Before("@annotation(Filter)")
	
	public Object FilterBadWords(ProceedingJoinPoint joinPoint) throws Throwable
	{
		String[] filteredSwearWords = new String[]{"mist"}; // You could insert actual swear words here
		
		PublishMessageCmd messageCmd = (PublishMessageCmd) joinPoint.getArgs()[0];
		String lowerMessage = messageCmd.getMessage().toLowerCase();

        for(String word : filteredSwearWords){

            int wordIndex = lowerMessage.indexOf(word);
            if (wordIndex >= 0){

                messageCmd.setMessage("****");
            }
        }
        
        return joinPoint.proceed(new Object[] {messageCmd});
	}
	
}
