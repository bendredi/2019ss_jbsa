package de.thi.jbsa.main.eventreceiver.persistence;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.NotEmpty;
import java.time.ZonedDateTime;

@Entity(name = "MessageEvent")
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
@Builder
public class MessageEventEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String channelId;

    @Lob
    private String event;

    @CreatedDate
    private ZonedDateTime created;

}
