package de.thi.jbsa.main.eventreceiver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.thi.jbsa.main.domain.PublishMessageCmd;
import de.thi.jbsa.main.eventreceiver.event.PublishedMessageEvent;
import de.thi.jbsa.main.eventreceiver.persistence.MessageEventEntity;
import de.thi.jbsa.main.eventreceiver.persistence.MessageEventRepository;
import de.thi.jbsa.main.wordfilter.Filter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessagePublisherListener {

	private final MessageEventRepository messageEventRepository;
	private final List<MessagePreProcessor> messagePreProcessors;
	private final ObjectMapper objectMapper;
	private final ApplicationEventPublisher applicationEventPublisher;

	@EventListener(PublishMessageCmd.class)
	@Filter
	public void onPublishMessageCmd(PublishMessageCmd cmd) {
        log.info("Message {} recieved", cmd.getMessage());

		messageEventRepository.save(toMessageEventEntity(cmd));

		applicationEventPublisher.publishEvent(toPublishMessageEvent(cmd));
	}

	private MessageEventEntity toMessageEventEntity(PublishMessageCmd publishMessageCmd) {

		MessageEventEntity entity = new MessageEventEntity();

		entity.setChannelId(publishMessageCmd.getChannelId());

		try {
			entity.setEvent(objectMapper.writeValueAsString(publishMessageCmd));
		} catch (JsonProcessingException e) {
			throw new RuntimeException("PublishMessageCmd coulstreamd not be mapped to JSON");
		}

		return entity;
	}

	private PublishedMessageEvent toPublishMessageEvent(PublishMessageCmd publishMessageCmd) {

		PublishedMessageEvent publishedMessageEvent = new PublishedMessageEvent();

		publishedMessageEvent.setMessage(publishMessageCmd.getMessage());
		publishedMessageEvent.setUser(publishMessageCmd.getUserId());
		publishedMessageEvent.setChannel(publishMessageCmd.getChannelId());

		return publishedMessageEvent;
	}
}
