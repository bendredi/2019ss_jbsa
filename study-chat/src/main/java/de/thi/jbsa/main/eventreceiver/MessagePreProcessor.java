package de.thi.jbsa.main.eventreceiver;

import de.thi.jbsa.main.domain.PublishMessageCmd;

public interface MessagePreProcessor {

    /**
     * Preprocesses a published message.
     * @param publishMessageCmd The published message to process.
     * @return {@code false} if publishing the message should be canceled. Otherwise {@code true}
     */
    boolean processPublishMessageCmd(PublishMessageCmd publishMessageCmd);
}
