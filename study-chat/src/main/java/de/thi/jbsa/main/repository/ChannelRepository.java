package de.thi.jbsa.main.repository;

import de.thi.jbsa.main.domain.Channel;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-nosql.html#boot-features-spring-data-mongo-repositories
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */

public interface ChannelRepository extends MongoRepository<Channel, Long> {

}
