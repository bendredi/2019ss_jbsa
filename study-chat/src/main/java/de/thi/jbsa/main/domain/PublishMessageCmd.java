package de.thi.jbsa.main.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
public class PublishMessageCmd extends Event {
	private String message;
	private String userId;
	private String channelId;

	public PublishMessageCmd() {
		super(EventType.PUBLISH_MESSAGE);
	}
}
