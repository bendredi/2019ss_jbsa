package de.thi.jbsa.main.eventreceiver.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessageEventRepository extends CrudRepository<MessageEventEntity, Long> {
    List<MessageEventEntity> findAllByChannelId(String channelId);
}
