package de.thi.jbsa.main.synchroniser;

import de.thi.jbsa.main.domain.Event;
import de.thi.jbsa.main.repository.CityRepository;
import de.thi.jbsa.main.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Synchroniser {

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private EventRepository eventRepository;
	
	@JmsListener(destination = "events", containerFactory = "myFactory")
	public void receiveEvent(Event event) {
		// TODO Add synchroniser actions
	}
}
