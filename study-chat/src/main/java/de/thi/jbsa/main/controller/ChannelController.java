package de.thi.jbsa.main.controller;

import de.thi.jbsa.main.repository.ChannelRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ChannelController {

	private final ChannelRepository channelRepository;

	@GetMapping({ "", "index", "index.html", "/" })
	public String getIndexSite(Model model) {
		model.addAttribute("channelId", "");
		return "index";
	}
}
