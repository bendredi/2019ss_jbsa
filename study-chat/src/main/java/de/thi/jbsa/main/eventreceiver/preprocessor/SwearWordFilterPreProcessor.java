package de.thi.jbsa.main.eventreceiver.preprocessor;

import de.thi.jbsa.main.domain.PublishMessageCmd;
import de.thi.jbsa.main.eventreceiver.MessagePreProcessor;
import org.springframework.stereotype.Component;


/**
 * Example for business logic in a chat server application.
 */
@Component
public class SwearWordFilterPreProcessor implements MessagePreProcessor {

    private String[] filteredSwearWords = new String[]{"mist"}; // You could insert actual swear words here

    @Override
    public boolean processPublishMessageCmd(PublishMessageCmd publishMessageCmd) {

        String lowerMessage = publishMessageCmd.getMessage().toLowerCase();

        for(String word : filteredSwearWords){

            int wordIndex = lowerMessage.indexOf(word);
            if (wordIndex >= 0){

                publishMessageCmd.setMessage("****");
                return true;
            }
        }

        return true;
    }
}
