package de.thi.jbsa.main.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private Long id;
    private String messageText;
    private LocalDateTime dateSent;

}
