package de.thi.jbsa.main.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.thi.jbsa.main.domain.EventType;
import de.thi.jbsa.main.domain.PublishMessageCmd;
import de.thi.jbsa.main.eventreceiver.persistence.MessageEventEntity;
import de.thi.jbsa.main.eventreceiver.persistence.MessageEventRepository;
import de.thi.jbsa.main.repository.ChannelRepository;
import de.thi.jbsa.main.service.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Controller
public class MessageController {
	private final ChannelRepository channelRepository;
	private final MessageEventRepository messageEventRepository;
	private final MessageService messageService;
	private final ObjectMapper objectMapper;

	@GetMapping("channel")
	public String redirect(@RequestParam final String channelId) {
		return "redirect:chat/" + channelId;
	}

	@GetMapping("chat/{channelId}")
	public String getOrCreateChannel(@PathVariable("channelId") final String channelId, final Model model) {
		final Iterable<MessageEventEntity> messages = messageService.getMessageEventEntities(channelId);

		model.addAttribute("channelId", channelId);
		model.addAttribute("messages", messages);
		return "/chat";
	}

	@PostMapping("chat/{channelId}")
	public String postMessage(@PathVariable final String channelId, @RequestParam("message") final String message,
			final Model model) {
		final PublishMessageCmd publishMessageCmd = new PublishMessageCmd();
		publishMessageCmd.setMessage(message)
				.setChannelId(channelId)
				.setUserId("1")
				.setEventType(EventType.PUBLISH_MESSAGE);
		messageService.publishMessage(publishMessageCmd);

		final List<PublishMessageCmd> messages = messageService.getMessageEventEntities(channelId)
				.stream()
				.map(messageEventEntity -> {
					try {
						return objectMapper.readValue(messageEventEntity.getEvent(), PublishMessageCmd.class);
					} catch (final IOException e) {
						e.printStackTrace();
					}
					return null;
				})
				.filter(Objects::nonNull)
                .collect(Collectors.toList());

		model.addAttribute("messages", messages);

		return "/chat";
	}
}
