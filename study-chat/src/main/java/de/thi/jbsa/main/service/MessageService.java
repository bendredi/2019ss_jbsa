package de.thi.jbsa.main.service;

import de.thi.jbsa.main.domain.PublishMessageCmd;
import de.thi.jbsa.main.eventreceiver.persistence.MessageEventEntity;
import de.thi.jbsa.main.eventreceiver.persistence.MessageEventRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessageService {
	private final ApplicationEventPublisher eventPublisher;
	private final MessageEventRepository messageEventRepository;

	public PublishMessageCmd publishMessage(final PublishMessageCmd publishMessageCmd) {
		eventPublisher.publishEvent(publishMessageCmd);
	    log.info("Message {} sent", publishMessageCmd.getMessage());
		return publishMessageCmd;
	}

	public List<MessageEventEntity> getMessageEventEntities(final String channelId) {
		return messageEventRepository.findAllByChannelId(channelId);
	}
}
