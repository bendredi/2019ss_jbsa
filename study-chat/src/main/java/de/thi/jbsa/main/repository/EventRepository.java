package de.thi.jbsa.main.repository;

import de.thi.jbsa.main.domain.Event;
import de.thi.jbsa.main.domain.EventType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */
public interface EventRepository extends CrudRepository<Event, Long> {

  List<Event> findByEventType(EventType eventType);

}
