package de.thi.jbsa2019ss.demo.model;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class UserCounter {

    private long cnt = 0;

}
