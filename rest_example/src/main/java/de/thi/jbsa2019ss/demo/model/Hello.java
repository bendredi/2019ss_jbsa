package de.thi.jbsa2019ss.demo.model;

import lombok.Data;

@Data
public class Hello {

    private long id;
    private String content;

    public Hello(long id, String content) {
        this.id = id;
        this.content = content;
    }
}
