package de.thi.jbsa2019ss.demo.rest;

import com.google.gson.Gson;
import de.thi.jbsa2019ss.demo.model.Hello;
import de.thi.jbsa2019ss.demo.model.UserCounter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;


@RestController
public class RestApiController {

    private static final Logger LOG = LoggerFactory.getLogger(RestApiController.class);
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    private Gson gson = new Gson();

    @Autowired
    private UserCounter userCounter;

    @PostMapping(value = "/hello")
    public ResponseEntity<String> helloApi(@RequestBody String name) {
        userCounter.setCnt(userCounter.getCnt() + 1);
        String customer = gson.fromJson(name, String.class);
        LOG.info("Counter: {} and Name: {}", userCounter.getCnt(), customer);

        return new ResponseEntity<String>("Hello " + customer + ". Counter is at: " + userCounter.getCnt(), HttpStatus.OK);
    }

    @RequestMapping("/welcome")
    public Hello welcome(@RequestParam(value = "name", defaultValue = "World") String name) {
        userCounter.setCnt(userCounter.getCnt() + 1);
        LOG.info("Counter: {} and Name: {}", userCounter.getCnt(), name);
        return new Hello(counter.incrementAndGet(), String.format(template, name));
    }
}
