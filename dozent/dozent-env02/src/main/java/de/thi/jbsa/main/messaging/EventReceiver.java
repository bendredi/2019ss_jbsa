package de.thi.jbsa.main.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import de.thi.jbsa.main.domain.ChangeInhabitantEvent;
import de.thi.jbsa.main.domain.City;
//import de.thi.jbsa.main.domain.CreateCityEvent;
import de.thi.jbsa.main.domain.CreateCityEvent;
import de.thi.jbsa.main.domain.Event;
import de.thi.jbsa.main.repository.CityRepository;
import de.thi.jbsa.main.repository.EventRepository;

/**
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */
@Component
public class EventReceiver {

  private final CityRepository cityRepository;

  private final EventRepository eventRepository;

  @Autowired
  public EventReceiver(EventRepository eventRepository, CityRepository cityRepository) {
    this.eventRepository = eventRepository;
    this.cityRepository = cityRepository;
  }

  @JmsListener(destination = "events", containerFactory = "myFactory")
  public void receiveEvent(Event event) {
    eventRepository.save(event);
    // The following is event processing/event sourcing. Could just as well happen in a createCityResult-Service
    if (event instanceof CreateCityEvent) {
      CreateCityEvent createCityEvent = (CreateCityEvent) event;

      if (cityRepository.findByName(createCityEvent.getName()) != null) {
        // this city exists. Saving will work, but the next request will not return a usage result.
        // We need to throw an exception here and inform the user, that the requested action could not be processed.
      }
      City newCity = new City();
      newCity.setName(createCityEvent.getName());
      newCity.setInhabitants(createCityEvent.getInhabitantNumber());
      cityRepository.save(newCity);
    }

    if (event instanceof ChangeInhabitantEvent) {
      City affectedCity = cityRepository.findByName(((ChangeInhabitantEvent) event).getAffectedCity());
      // The new city would just be saved besides the current one, so we delete the current one and then add a new one.
      cityRepository.deleteByName(affectedCity.getName());
      affectedCity.setInhabitants(((ChangeInhabitantEvent) event).getNewInhabitantNumber());
      cityRepository.save(affectedCity);
    }
  }
}
