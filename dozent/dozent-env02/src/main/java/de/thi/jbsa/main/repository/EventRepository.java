package de.thi.jbsa.main.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import de.thi.jbsa.main.domain.Event;
import de.thi.jbsa.main.domain.EventType;

/**
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */
public interface EventRepository extends CrudRepository<Event, Long> {

  List<Event> findByEventType(EventType eventType);

}
