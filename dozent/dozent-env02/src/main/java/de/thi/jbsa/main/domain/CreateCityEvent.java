package de.thi.jbsa.main.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "event")
@DiscriminatorValue("createCity")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class CreateCityEvent
  extends Event {

  private Long inhabitantNumber;

  @NotNull
  private String name;

  public CreateCityEvent() {
    super(EventType.CREATE_CITY);
  }

  public Long getInhabitantNumber() {
    return inhabitantNumber;
  }

  public String getName() {
    return name;
  }

  public void setInhabitantNumber(Long inhabitantNumber) {
    this.inhabitantNumber = inhabitantNumber;
  }

  public void setName(String name) {
    this.name = name;
  }
}