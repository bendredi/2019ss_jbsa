package de.thi.jbsa.main.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "event")
@DiscriminatorValue("changeInhabitant")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ChangeInhabitantEvent
  extends Event {

  @NotNull
  private String affectedCity;

  @NotNull
  private Long newInhabitantNumber;

  public ChangeInhabitantEvent() {
    super(EventType.CHANGE_CITY_INHABITANTS);
  }

  public String getAffectedCity() {
    return affectedCity;
  }

  public Long getNewInhabitantNumber() {
    return newInhabitantNumber;
  }

  public void setAffectedCity(String affectedCity) {
    this.affectedCity = affectedCity;
  }

  public void setNewInhabitantNumber(Long newInhabitantNumber) {
    this.newInhabitantNumber = newInhabitantNumber;
  }
}
