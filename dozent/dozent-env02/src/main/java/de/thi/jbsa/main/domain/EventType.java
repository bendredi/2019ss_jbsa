package de.thi.jbsa.main.domain;

/**
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */
public enum EventType {

  CHANGE_CITY_INHABITANTS,
  CREATE_CITY,

}
