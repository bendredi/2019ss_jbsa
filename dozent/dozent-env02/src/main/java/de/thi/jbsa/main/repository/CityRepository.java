package de.thi.jbsa.main.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import de.thi.jbsa.main.domain.City;

/**
 * https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-nosql.html#boot-features-spring-data-mongo-repositories
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */

public interface CityRepository
  extends MongoRepository<City, Long> {

  City findByName(String name);

  void deleteByName(String name);
}
