package de.thi.jbsa.main.messaging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import de.thi.jbsa.main.domain.ChangeInhabitantEvent;
import de.thi.jbsa.main.domain.City;
//import de.thi.jbsa.main.domain.CreateCityEvent;
import de.thi.jbsa.main.domain.CreateCityEvent;
import de.thi.jbsa.main.repository.CityRepository;
import de.thi.jbsa.main.repository.EventRepository;

/**
 * @author Christopher Timm <ct@contimm.de> on 04.05.19
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class EventReceiverTest {

  @Autowired
  private CityRepository cityRepository;

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private JmsTemplate jmsTemplate;


  @Test
  public void createCityEventTest()
    throws Exception {
    CreateCityEvent createCityEvent = new CreateCityEvent();
    createCityEvent.setInhabitantNumber(100L);
    createCityEvent.setName("Hinterpusemuckel");
    jmsTemplate.convertAndSend("events",createCityEvent);

    Thread.sleep(1000);

    City dbCity = cityRepository.findByName("Hinterpusemuckel");
    assertNotNull(dbCity);
    assertEquals((Long)100L, dbCity.getInhabitants());

  }

  @Test
  public void UpdateExistingCity() throws Exception{

    City city = new City();
    city.setName("Ingolstadt");
    city.setInhabitants(100000L);

    cityRepository.save(city);

    ChangeInhabitantEvent event = new ChangeInhabitantEvent();
    event.setAffectedCity("Ingolstadt");
    event.setNewInhabitantNumber(200000L);

    jmsTemplate.convertAndSend("events", event);

    // This is asynchronous, we need to wait for the event to be received and processed by the queue.
    Thread.sleep(1000);

    City finalCity = cityRepository.findByName("Ingolstadt");
    assertEquals((Long) 200000L, finalCity.getInhabitants());
  }

  @After
  public void tearDown()
    throws Exception {
    cityRepository.deleteAll();
    eventRepository.deleteAll();
  }


}