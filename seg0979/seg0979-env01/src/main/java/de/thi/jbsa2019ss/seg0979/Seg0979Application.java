package de.thi.jbsa2019ss.seg0979;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Seg0979Application {

	public static void main(String[] args) {
		SpringApplication.run(Seg0979Application.class, args);
	}

}
